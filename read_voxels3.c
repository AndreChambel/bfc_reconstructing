#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "omp.h"

float *voxel_med;


float correlation(float*voxel1, float*voxel2, int n, int k, int j) {

   float r = 0.0, xbar = 0.0, ybar = 0.0, sx = 0.0, sy = 0.0, sxy=0.0;

   float x[n], y[n];
   int i;

   for(i = 0; i < n; ++i) {
	  sxy += (voxel1[i] - voxel_med[k]) * (voxel2[i] - voxel_med[j]);
      sx += (voxel1[i] - voxel_med[k]) * (voxel1[i] - voxel_med[k]);
	  sy += (voxel2[i] - voxel_med[j]) * (voxel2[i] - voxel_med[j]);
   }
   sxy /= n;
   sx = sqrt((sx / n));
   sy = sqrt((sy / n));

   r = sxy / (sx * sy);

   return r;

}

void calcula_media(float *voxel, int n, int j){
	int i;
	voxel_med[j] = 0;
	for(i=0; i<n; i++){
		voxel_med[j] += voxel[i];
	}
	voxel_med[j] /= n;
}


int main(int argc, char *argv[]) {

	char filename[50], number[10], time[10];
	int numb, t,i,j, **pairs, connections=0, total=0;
	float **voxels,c;
	
	FILE * fid;

	if(argc < 2){
		printf("not enought arguments, usage ./<program> <filename> \n");
		exit(-1);
	}
	
	strcpy(filename,argv[1]);
	
	fid = fopen(filename,"r");
	
	fscanf(fid,"%s %s", number, time);
	t = atoi(time);
	numb = atoi(number);
	
	printf("%d voxels in %d timepoints \n", numb, t);
	
	voxels = (float**) malloc(numb*sizeof(float*));
	puts("a ler ficheiro\n");
	for(i=0; i<numb; i++){
		voxels[i] = (float*) malloc(t*sizeof(float));
		for(j=0; j<t; j++){
			fscanf(fid, "%f", &voxels[i][j]);
		}
	}
	
	voxel_med = (float *) malloc(numb*sizeof(float));
	
	puts("a calcular medias\n");
	for(i=0;i<numb;i++){
		calcula_media(voxels[i],t,i);
	}

	puts("a calcular ligacoes\n");
	
	#pragma omp parallel private(i,j,c)
	{
	
	#pragma omp for schedule(dynamic) reduction(+:connections)
	for(i=0;i<numb;i++){
		for(j=i;j<numb;j++){
			c = correlation(voxels[i],voxels[j],t,i,j);
			if(c >= 0.4){
				connections++;
			}	
		}
		printf("%d\n",i);
	}
	
	#pragma omp single
	printf("number of connections = %d\n",connections);
	
	
	}
	
	
	exit(0);

}




// dynamic - 2102m40